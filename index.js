// console.log("qwe")

// getCube
const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`)

// address
let address = ['258', 'Washington Ave NW', 'California', '90011'];
const [houseNumber, street, state, postalCode] = address;
	console.log(`I live at ${houseNumber} ${street}, ${state} ${postalCode}.`)

// animal
const animal = {
	name: 'Lolong',
	species: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 30 in'
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)

// numbers
let numbers = [1, 2, 3, 4, 5];
numbers.forEach(numbers => console.log(numbers));

// total number
const reduceNumber = numbers.reduce((accumulator, currentValue) => {
	return accumulator + currentValue;
}, 0);
console.log(reduceNumber)

//dog
class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
	}
}
let dogDetails = new Dog('Akamaru', '10', 'Askal')
	console.log(dogDetails);